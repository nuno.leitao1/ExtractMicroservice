# Entity Extraction Service

## Setting up the project...

To setup the structure of this project, the following was used:

```
dotnet new sln -o ExtractMicroservice
dotnet sln add Extract/Extract.csproj
dotnet new xunit -o Extract.Tests
dotnet sln add Extract.Tests/Extract.Tests.csproj
dotnet add Extract.Tests/Extract.Tests.csproj reference Extract/Extract.csproj
```

## What is this ?

This is an example (for personal educational purposes only) .NET Core entity extractor. It highlights a few best practices of writing API services in .NET, including _Dependency Injection_ (inversion of control), unit testing with xUnit, and Aspect Oriented Programming (AOP) with the PostSharp framework (implementing app wide caching and logging, avoiding boilerplate code). It also comes with a Docker configuration which will allow you to run the service within a Docker container, ready for deployment under Kubernetes.

## Building and running

Easiest is to use *Visual Studio Code* or *Visual Studio* for most tasks, but you can also build and run manually:

To list which packages the project uses (via the `nuget` package manager):

```shell
dotnet list package
```

To run all unit tests:

```shell
dotnet test
```

and then to build and run:

```shell
dotnet build
```

```shell
dotnet run --project Extract/Extract.csproj
```

The application will work on just about any platform which supports .NET (Windows, Linux, MacOS, WSL, ...).

## Building and running on containers

### With docker-compose

Both Jaeger for tracing, and the extract service can be containerized using composition:

```shell
docker compose build
```
```shell
docker compose pull
```
```shell
docker compose up
```

Or alternatively you can build the service and run on a container individually...

(also see below how to instantiate requests to the service, and look at traces)

### Individually

To build a Docker image which you can run as a container, either run `Docker Images: Build Image` as a Visual Studio Code command, or manually:

```shell
cd Extract; docker build --rm --pull -f "Dockerfile" -t "extract-api:latest" "."
```

You will also want to create a bridge network for the service to communicate with other components:

```shell
docker network create extract-core
```

While to run the container:

```shell
docker run --rm -d --net extract-core -p 5110:5110/tcp extract-api:latest
```

## Accessing the API

Just point your browser at http://localhost:5110/swagger, or you can curl your way into the API directly, example:

```shell
curl -X 'GET' \
  'http://localhost:5110/Extractor?text=Is%20%2B44%207905%20560111%20a%20phone%20number%20%3F%20What%20about%20N167AA%20%3F' \
  -H 'accept: text/plain'
```

## Steeltoe

This app integrates various Steeltoe components (Distributed tracing, health and discovery), to make it more cloud ready. Of note, all management endpoints are exposed (http://localhost:5110/actuator/health, http://localhost:5110/actuator/info, etc.), and OpenTelemetry tracing is enabled. As part of this example, you can run a Jaeger docker image and see traces being generated:

```shell
docker run -d --rm --net extract-core --name jaeger \
  -e COLLECTOR_ZIPKIN_HOST_PORT=:9411 \
  -p 5775:5775/udp \
  -p 6831:6831/udp \
  -p 6832:6832/udp \
  -p 5778:5778 \
  -p 16686:16686 \
  -p 14250:14250 \
  -p 14268:14268 \
  -p 14269:14269 \
  -p 9411:9411 \
  jaegertracing/all-in-one
  ```

  And then point your browser at http://localhost:16686 for the Jaeger interface where you'll be able to inspect traces.

## PostSharp
This app also uses injection to realise Aspect Oriented Programming (AOP) with the PostSharp metaprogramming framework, you will see the Decorators `[Log]` and `[Cache]` which are handled by PostSharp.

## Learning notes and concepts

- Inversion of Control (IoC) and Dependency Injection: https://stackoverflow.com/questions/6550700/inversion-of-control-vs-dependency-injection
- Dependency Injection in .NET Core: https://softchris.github.io/pages/dotnet-di.html#what-is-dependency-injection
- Spring Boot vs .NET Core: https://medium.com/@putuprema/spring-boot-vs-asp-net-core-a-showdown-1d38b89c6c2d
- .NET Core in a Container: https://code.visualstudio.com/docs/containers/quickstart-aspnet-core
- Intro to Steeltoe (https://docs.steeltoe.io/guides/get-to-know-steeltoe/exercise1.html?tabs=visual-studio)
- Steeltoe management endpoints: https://docs.steeltoe.io/api/v3/management/using-endpoints.html
- Steeltoe tracing: https://docs.steeltoe.io/api/v3/tracing/
- Setting up a .NET project from scratch, including xUnit testing: https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test
- PostSharp, a great AOP implementation (not free): https://www.postsharp.net/postsharp
