using System.Drawing;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OpenTelemetry.Exporter;
using OpenTelemetry;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.Caching.Memory;
using Steeltoe.Management.Endpoint;
using Steeltoe.Management.Tracing;
using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Caching.Backends;
using PostSharp.Patterns.Diagnostics;
using Extract.Extractors;

namespace Extract
{
    public class Startup
    {
        private IMemoryCache cache = new MemoryCache(new MemoryCacheOptions());

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            CachingServices.DefaultBackend = new MemoryCacheBackend(cache);
            LoggingServices.DefaultBackend = new PostSharp.Patterns.Diagnostics.Backends.Console.ConsoleLoggingBackend();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IExtractorService, PhoneExtractor>();
            services.AddScoped<IExtractorService, PostcodeExtractor>();
            services.AddScoped<IExtractorService, EmailExtractor>();
            services.AddAllActuators(Configuration);
            services.AddDistributedTracingAspNetCore();
            // We are doing this configuration in code, because the Steeltoe documentation
            // is a total mess when it comes to tracing. Ideally all this should happen
            // in appsettings.json.
            services.PostConfigure<JaegerExporterOptions>(options =>
                {
                    options.ExportProcessorType = ExportProcessorType.Batch;
                    options.Protocol = JaegerExportProtocol.HttpBinaryThrift;
                    options.Endpoint = new System.Uri("http://jaeger:14268");
                    options.BatchExportProcessorOptions.ExporterTimeoutMilliseconds = 1000;
                });
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Extract", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Extract"));
            }
            
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapAllActuators(null);
            });
        }
    }
}
