namespace Extract.Extractors;

using System.Text.RegularExpressions;
using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Diagnostics;

public class EmailExtractor : IExtractorService
{
    private string expression = @"[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})";
   
    [Cache]
    [Log]
    public List<Entity> Extract(string text)
    {
        List<Entity> entities = new List<Entity>();

        MatchCollection matches = Regex.Matches(text, expression, RegexOptions.Multiline);
        foreach (Match match in matches)
        {
            entities.Add(new Entity
            {
                Type = "email",
                Unnormalized = match.Value,
                Normalized = match.Value,
                Confidence = 1.0
            });
        }

        return entities;
    }
}