namespace Extract.Extractors;

using System.Text.RegularExpressions;
using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Diagnostics;

public class PhoneExtractor : IExtractorService
{
    private string expression = @"[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*";

    [Cache]
    [Log]
    public List<Entity> Extract(string text)
    {
        List<Entity> entities = new List<Entity>();

        MatchCollection matches = Regex.Matches(text, expression, RegexOptions.Multiline);
        foreach (Match match in matches)
        {
            entities.Add(new Entity {
                Type = "phone",
                Unnormalized = match.Value,
                Normalized = match.Value.Replace(".","").Replace("-","").Replace(" ",""),
                Confidence = 1.0
            });
        }

        return entities;
    }
}