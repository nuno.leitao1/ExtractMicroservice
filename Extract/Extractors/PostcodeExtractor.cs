namespace Extract.Extractors;

using System.Text.RegularExpressions;
using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Diagnostics;

public class PostcodeExtractor : IExtractorService
{
    private string expression = @"(([A-Z][A-HJ-Y]?\d[A-Z\d]?|ASCN|STHL|TDCU|BBND|[BFS]IQQ|PCRN|TKCA) ?\d[A-Z]{2}|BFPO ?\d{1,4}|(KY\d|MSR|VG|AI)[ -]?\d{4}|[A-Z]{2} ?\d{2}|GE ?CX|GIR ?0A{2}|SAN ?TA1)";

    [Cache]
    [Log]
    public List<Entity> Extract(string text)
    {
        List<Entity> entities = new List<Entity>();

        MatchCollection matches = Regex.Matches(text, expression, RegexOptions.Multiline);
        foreach (Match match in matches)
        {
            entities.Add(new Entity
            {
                Type = "postcode",
                Unnormalized = match.Value,
                Normalized = match.Value.Replace(".", "").Replace("-", "").Replace(" ", ""),
                Confidence = 1.0
            });
        }

        return entities;
    }
}