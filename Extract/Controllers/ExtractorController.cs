using Microsoft.AspNetCore.Mvc;
using Extract.Extractors;
using PostSharp.Patterns.Diagnostics;

namespace Extract.Controllers;

[ApiController]
[Route("[controller]")]
public class ExtractorController : ControllerBase
{

    private readonly ILogger<ExtractorController> _logger;
    private readonly IEnumerable<IExtractorService> _extractorServices;

    public ExtractorController(ILogger<ExtractorController> logger, IEnumerable<IExtractorService> extractorServices)
    {
        _logger = logger;
        _extractorServices = extractorServices;
    }

    [HttpGet(Name = "Extractor")]
    [Log]
    public IEnumerable<Entity> Get([FromQuery]string text)
    {
        List<Entity> entities = new List<Entity>();

        foreach (IExtractorService extractor in _extractorServices)
        {
            entities.AddRange(extractor.Extract(text));
        }
        return entities.ToArray();
    }

    [HttpPost(Name = "Extractor")]
    [Log]
    public IEnumerable<Entity> Post([FromBody]string text)
    {
        List<Entity> entities = new List<Entity>();

        foreach (IExtractorService extractor in _extractorServices)
        {
            entities.AddRange(extractor.Extract(text));
        }
        return entities.ToArray();
    }
}
