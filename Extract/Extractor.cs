namespace Extract;

public struct Entity
{
    public string Type { get; set; }
    public string Unnormalized { get; set; }
    public string Normalized { get; set; }
    public double Confidence { get; set; }
}

public interface IExtractorService
{
    public List<Entity> Extract(string text);
}