using System;
using System.Linq;
using Xunit;
using Extract;
using Extract.Extractors;
using Microsoft.Extensions.Caching.Memory;
using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Caching.Backends;
using PostSharp.Patterns.Diagnostics;

namespace ExtractTests;

public class TestPostcodeExtractor
{
    private IMemoryCache cache = new MemoryCache(new MemoryCacheOptions());
    public TestPostcodeExtractor()
    {
        CachingServices.DefaultBackend = new MemoryCacheBackend(cache);
        LoggingServices.DefaultBackend = new PostSharp.Patterns.Diagnostics.Backends.Console.ConsoleLoggingBackend();
    }

    [Theory]
    [InlineData("A012BC")]
    [InlineData("W15AB")]
    public void Extract_ReturnsOne(string text)
    {
        IExtractorService extractor = new PostcodeExtractor();
        Assert.Single(extractor.Extract(text));
    }

    [Theory]
    [InlineData("A012BC,W15AB")]
    public void Extract_ReturnsTwo(string text)
    {
        IExtractorService extractor = new PostcodeExtractor();
        Assert.Equal(2, extractor.Extract(text).Count());
    }
}