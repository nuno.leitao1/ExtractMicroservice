using System.Linq;
using Xunit;
using Extract;
using Extract.Extractors;
using Microsoft.Extensions.Caching.Memory;
using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Caching.Backends;
using PostSharp.Patterns.Diagnostics;

namespace ExtractTests;

public class TestEmailExtractor
{
    private IMemoryCache cache = new MemoryCache(new MemoryCacheOptions());
    public TestEmailExtractor() {
        CachingServices.DefaultBackend = new MemoryCacheBackend(cache);
        LoggingServices.DefaultBackend = new PostSharp.Patterns.Diagnostics.Backends.Console.ConsoleLoggingBackend();
    }
    
    [Theory]
    [InlineData("this.email@domain.org")]
    [InlineData("this.email+01@domain.org")]
    public void Extract_ReturnsOne(string text)
    {
        IExtractorService extractor = new EmailExtractor();
        Assert.Single(extractor.Extract(text));
    }

    [Theory]
    [InlineData("this.email@domain.org,this.email+01@domain.org")]
    public void Extract_ReturnsTwo(string text)
    {
        IExtractorService extractor = new EmailExtractor();
        Assert.Equal(2, extractor.Extract(text).Count());
    }
}