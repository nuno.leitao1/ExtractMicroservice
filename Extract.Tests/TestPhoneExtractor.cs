using System.Linq;
using Xunit;
using Extract;
using Extract.Extractors;
using Microsoft.Extensions.Caching.Memory;
using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Caching.Backends;
using PostSharp.Patterns.Diagnostics;

namespace ExtractTests;

public class TestPhoneExtractor
{
    private IMemoryCache cache = new MemoryCache(new MemoryCacheOptions());
    public TestPhoneExtractor()
    {
        CachingServices.DefaultBackend = new MemoryCacheBackend(cache);
        LoggingServices.DefaultBackend = new PostSharp.Patterns.Diagnostics.Backends.Console.ConsoleLoggingBackend();
    }

    [Theory]
    [InlineData("+4411112223333")]
    [InlineData("+44-1111-222-3333")]
    public void Extract_ReturnsOne(string text)
    {
        IExtractorService extractor = new PhoneExtractor();
        Assert.Single(extractor.Extract(text));
    }

    [Theory]
    [InlineData("+4411112223333,+44-1111-222-3333")]
    public void Extract_ReturnsTwo(string text)
    {
        IExtractorService extractor = new PhoneExtractor();
        Assert.Equal(2, extractor.Extract(text).Count());
    }
}